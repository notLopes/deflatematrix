package ccd.trab.Deflate;

import java.io.*;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class Inflate {


    private static final int DEFAULT_LAB_LENGTH_SIZE = 2^8; //256 Bytes
    private Inflater inflater;

    public Inflate() {
        inflater = new Inflater();
    }

    public long inflate(String file, String dictionaryPath, String outputPath) throws IOException, DataFormatException {

        try{
            byte[] output = new byte[DEFAULT_LAB_LENGTH_SIZE];
            byte[] fileInBytes = readFile(file), dictionary = null;

            if(dictionaryPath != null)
                dictionary = readFile(dictionaryPath);

            inflater.setInput(fileInBytes, 0, fileInBytes.length);

            OutputStream writer = new FileOutputStream(outputPath);
            int writtenBytes;
            while((writtenBytes = inflater.inflate(output)) > 0 || inflater.needsDictionary()) {
                if(inflater.needsDictionary() && dictionary != null)
                    inflater.setDictionary(dictionary);
                else
                    writer.write(output, 0, writtenBytes);
            }

            writer.close();

            long total = inflater.getBytesWritten();
            return total;
        } finally {
            inflater.end();
        }
    }

    private byte[] readFile(String file) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream reader = new FileInputStream(file);

        byte[] current = new byte[DEFAULT_LAB_LENGTH_SIZE];
        int bytesRead;
        while((bytesRead = reader.read(current)) != -1)
            baos.write(current, 0, bytesRead);

        return baos.toByteArray();
    }
}
