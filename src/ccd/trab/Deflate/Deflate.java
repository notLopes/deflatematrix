package ccd.trab.Deflate;

import java.io.*;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class Deflate {

    private static final int DEFAULT_LAB_LENGTH_SIZE = 2^8; //256 Bytes
    private InputStream is;
    private Deflater deflater;

    public Deflate() {
        deflater = new Deflater();
        deflater.setLevel(Deflater.NO_COMPRESSION);
    }

    public void setDictionary(String path) throws IOException {
        InputStream reader = new FileInputStream(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] current = new byte[DEFAULT_LAB_LENGTH_SIZE];
        int bytesRead;

        //Reading bytes from file
        while((bytesRead = reader.read(current)) != -1)
            baos.write(current, 0, bytesRead);

        //Setting dictionary for stream
        byte[] dictionary = baos.toByteArray();
        baos.close();
        deflater.setDictionary(dictionary, 0, dictionary.length);

        reader.close();
    }

    public long deflate(String file, String outputPath) throws IOException {

        try{
            byte[] output = new byte[DEFAULT_LAB_LENGTH_SIZE];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            InputStream reader = new FileInputStream(file);

            byte[] current = new byte[DEFAULT_LAB_LENGTH_SIZE];
            int bytesRead;
            while((bytesRead = reader.read(current)) != -1)
                baos.write(current, 0, bytesRead);

            byte[] fileInBytes = baos.toByteArray();

            deflater.setInput(fileInBytes, 0, fileInBytes.length);
            deflater.finish();

            OutputStream writer = new FileOutputStream(outputPath);
            int writtenBytes;

            while((writtenBytes = deflater.deflate(output)) > 0)
                writer.write(output,0, writtenBytes);

            writer.close();

            long total = deflater.getBytesWritten();
            return total;
        } finally {
            deflater.end();
        }
    }

    public static int inflate(String filePath,
                              String dictionaryPath,
                              String output)
            throws IOException, DataFormatException
    {
        Inflater inflater = new Inflater();
        try {
            byte[] bytRead = new byte[DEFAULT_LAB_LENGTH_SIZE];
            byte[] dictionary = null;
            if(dictionaryPath != null){
                InputStream reader = new FileInputStream(dictionaryPath);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                byte[] current = new byte[DEFAULT_LAB_LENGTH_SIZE];
                int bytesRead;

                //Reading bytes from file
                while((bytesRead = reader.read(current)) != -1)
                    baos.write(current, 0, bytesRead);

                //Setting dictionary for stream
                dictionary = baos.toByteArray();
                baos.close();

                reader.close();
            }

            InputStream is = new FileInputStream(filePath);

            int n;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((n = is.read(bytRead)) == -1)
                baos.write(bytRead, 0, n);

            inflater.setInput(baos.toByteArray(), 0, baos.size());
            is.close();

            OutputStream os = new FileOutputStream(output);
            byte[] bytesToWrite = new byte[DEFAULT_LAB_LENGTH_SIZE];
            do {
                if(inflater.needsDictionary() && dictionary != null) {
                    inflater.setDictionary(dictionary, 0, dictionary.length);
                }

                n = inflater.inflate(bytesToWrite, 0, DEFAULT_LAB_LENGTH_SIZE);
                os.write(bytesToWrite, 0, n);
            } while (n == DEFAULT_LAB_LENGTH_SIZE);

            os.close();

            return 0;
        } finally {
            inflater.end();
        }
    }
}
