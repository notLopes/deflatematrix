package ccd.trab.Deflate.LZW;

import javax.management.openmbean.InvalidOpenTypeException;
import java.util.ArrayList;

/**
 * LZW LZ77
 */
public class LZWDictionary {
    private final ArrayList<ByteSequence> dictionary = new ArrayList<>();


    /**
     * Gets the index of the dictionary, also known as token
     * @param seq - byte sequence
     * @return - token of the sequence in the dictionary or -1 if not in dictionary
     */
    public boolean contains(ByteSequence seq){
        return dictionary.contains(seq);
    }

    /**
     * Adds the sequence to the dictionary returning the token where it was put
     * @param seq - sequence of bytes to add
     * @return the index in byte[] format
     */
    public byte[] add(ByteSequence seq){
        dictionary.add(seq);
        return parseByteArray(dictionary.indexOf(seq));
    }

    /**
     * Gets a token in byte array
     * @param seq - sequence that might be inside the dictionary
     * @return -
     */
    public byte[] getToken(ByteSequence seq){
        int i = dictionary.indexOf(seq);
        //if(i == -1)
        //    throw new InvalidOpenTypeException("Cannot get a token of a sequence not inside the dictionary.");
        if(i == -1){
            return seq.getBytes();
        }

        return parseByteArray(i);
    }

    public ByteSequence getSequence(byte[] token){
        int i = 0;
        for (int j = 0; j < token.length; j++) {
            int current = (int)token[j] & 0xFF;
            i = i + (current << (j * 8));

        }
        return dictionary.get(i);
    }

    /**
     * Parses a integer into a byte[], that should be the token
     * used.
     * @param i - integer
     * @return a token
     */
    private byte[] parseByteArray(int i){
        final int BYTE_SIZE = 8;

        byte[] b = new byte[]{ (byte)(i),  (byte)(i >>> BYTE_SIZE), (byte)(i >>> (2 * BYTE_SIZE)), (byte)(i >>> (3 * BYTE_SIZE))};
        byte[] arr;
        int count = 0;
        for(int j = 0; j < b.length; j++){
            if(b[j] != 0) count = j + 1;
        }

        arr = new byte[count];

        for (int j = 0; j < arr.length; j++)
            arr[j] = b[j];

        return arr;
    }
}
