package ccd.trab.Deflate.LZW;

public class LZWTokenizer {

    public static void main(String[] args) {
	    if(args.length < 1)
	        throw new IllegalArgumentException("Please specify path of the input file.");

	    String path = args[0];
	    LZWEncoder encoder = new LZWEncoder();

        encoder.encode(path, path, path+".lzw");
    }
}
