package ccd.trab.Deflate.LZW;

import java.io.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class MainLZW {

    public static void main(String ... args) throws InterruptedException, ExecutionException, IOException {
        if(args.length < 1)
            throw new IllegalArgumentException("Please specify path.");

        File dir = new File(args[0]);
        if(!dir.isDirectory()) {
            throw new IllegalArgumentException("Please specify a directory path.");
        }

        File[] files = dir.listFiles();
        CompletableFuture<Result>[][] deflated = new CompletableFuture[files.length][files.length];

        Object object = new Object();
        int[] count = {0};
        PrintStream std = System.out;

        for (int i = 0; i < files.length; i++) {
            for (int j = 0; j < files.length; j++) {

                final int fi = i, fj = j;
                deflated[fi][fj] = CompletableFuture.supplyAsync(() -> {
                    try {
                        LZWEncoder lz = new LZWEncoder();
                        long initTime = System.currentTimeMillis();
                        long size = 0;
                        if(fi == fj) {
                            size = files[fi].length();
                        } else {
                            size = lz.encode(files[fj].getAbsolutePath(),
                                    files[fi].getAbsolutePath(),
                                    "res/lzw/" + files[fi].getName() + ".lzw");
                        }

                        long time = System.currentTimeMillis() - initTime;
                        std.println(time);
                        synchronized (object){ count[0] += time; }

                        return new Result(time, size);

                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                });
            }
        }

        if(args.length > 1) {
            File resFile = new File(args[1]);
            if(resFile.exists())
                resFile.delete();

            resFile.createNewFile();
            System.setOut(new PrintStream(new FileOutputStream(resFile)));
        }

        for (int i = 0; i < files.length; i++) {
            System.out.print(",");
            System.out.print(files[i].getName());
        }


        for (int i = 0; i < deflated.length; i++) {
            System.out.print("\n" + files[i].getName() + ",");

            for (int j = 0; j < deflated[i].length; j++) {
                Result res = deflated[i][j].join();

                System.out.print(res.bytes);
                if(j < deflated[i].length - 1)
                    System.out.print(",");
            }
        }

        System.out.println("\nFile,Best LZ77,Size,Worst LZ77,Size,No LZ77");
        for (int i = 0; i < files.length; i++) {
            System.out.print("\n" + files[i].getName());
            int best = 0, worst = 0;
            for (int j = 0; j < files.length; j++){
                long bestSize = deflated[i][best].get().bytes;
                long worstSize = deflated[i][worst].get().bytes;

                long size = deflated[i][j].get().bytes;

                if(size < bestSize)
                    best = j;
                else if(size > worstSize)
                    worst = j;
            }
            System.out.printf(",%s,%d,%s,%d,%d",
                    files[best].getName(), deflated[i][best].get().bytes,
                    files[worst].getName(), deflated[i][worst].get().bytes,
                    deflated[i][i].get().bytes);

        }

        System.setOut(std);
        System.out.println(count[0]);
    }

    private static class Result {
        public long time;
        public long bytes;

        Result(long time, long bytes) {
            this.time = time;
            this.bytes = bytes;
        }
    }

    private static String readFile(String file) throws IOException {
        FileInputStream is = new FileInputStream(file);
        byte [] bts = is.readAllBytes();
        return new String(bts);
    }
}
