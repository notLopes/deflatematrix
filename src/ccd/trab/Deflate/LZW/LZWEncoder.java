package ccd.trab.Deflate.LZW;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LZWEncoder {

    private final LZWDictionary dictionary = new LZWDictionary();

    /**
     * Encodes a file using LZW
     * @param path - File path
     * @param encodedPath - Encoded destination
     */
    int encode(String dictionary, String path, String encodedPath){
        int bytesInFile = 0;
        if(dictionary != null){
            bytesInFile = startDictionary(dictionary);
        }
        bytesInFile += startDictionary(path);
        List<byte[]> encodedFile = startEncoding(path);
        writeEncode(encodedPath, encodedFile, bytesInFile);
        return bytesInFile;
    }

    /**
     * Decodes a file given it's dictionary
     * @param path - file path
     * @param decodedPath - decoded file result path
     * @param dic - dictionary
     */
    static void decode(String path, String decodedPath, LZWDictionary dic){
        List<ByteSequence> decoded = startDecoding(path, dic);

        try(FileOutputStream fOs = new FileOutputStream(decodedPath)){
            for (ByteSequence seq: decoded)
                fOs.write(seq.getBytes());
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Used mainly for quick decoding
     * @return dictionary
     */
    LZWDictionary getDictionary() {
        return dictionary;
    }

    /**
     * Writes encoded file
     * @param list - list of objects
     */
    private void writeEncode(String path, List<byte[]> list, int count){
        try(FileOutputStream fOs = new FileOutputStream(path)){
            OutputStreamWriter osw = new OutputStreamWriter(fOs, "windows-1252");
            int current = 0;
            for (byte[] value: list){
                current += value.length; //How many bytes will be written
                //if(current == 190)
                //    System.out.println();
                String s = new String(value);
                osw.write(s + "\n");
                //osw.write(value);
                //osw.write((byte)'\n');
            }

            String compressionRate = "Original: " + count + ", Encoded file: " + current +
                    ", Compression Rate: " + (double)count/(double)current;
            osw.write(compressionRate);
            osw.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Starts dictionary putting every different byte from the file in the initial positions of the dictionary
     * (0 - 255)
     * @return number of bytes that InputStream read in byte[] form, in the first position of the list, needs parsing
     */
    private Integer startDictionary(String path){
        int count = 0;
        try(FileInputStream is = new FileInputStream(path)){
            byte b[] = new byte[8];
            int n;
            while ((n = is.read(b)) != -1) {
                count += n;
                ByteSequence bs = new ByteSequence(b);
                if (!dictionary.contains(bs))
                    dictionary.add(bs);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Encodes bytes in file putting the encoded sequences into a List instance
     * @return - list of bytes, that represent a token for a sequence
     */
    private  List<byte[]> startEncoding(String path){
        List<byte[]> output = new ArrayList<>();
        try(FileInputStream is = new FileInputStream(path)){
            byte[] previous = new byte[1];
            boolean reading = is.read(previous) > 0;
            ByteSequence currentSequence = new ByteSequence(previous);
            while (reading) {
                byte[] current = new byte[1];
                reading = is.read(current) > 0;

                //In case there are no more characters
                if (!reading) {
                    if (!currentSequence.equals(new ByteSequence())) {
                        //Encode the sequence in case the sequence is not null
                        byte[] token;
                        if(!dictionary.contains(currentSequence))
                            token = dictionary.add(currentSequence);
                        else token = dictionary.getToken(currentSequence);

                        output.add(token);
                    }
                    break;
                }

                //Make a disposable sequence, to put the sequence in the dictionary
                ByteSequence clone = new ByteSequence(currentSequence.getBytes());
                clone.append(current);
                if (dictionary.contains(clone)) {
                    //Current Sequence will add the next byte and see if the dictionary contains it
                    currentSequence = clone;
                } else {
                    //Adds sequence to the dictionary, if it's not in dictionary
                    dictionary.add(clone);

                    //Get the token for previous and put it in the output
                    byte[] token = dictionary.getToken(currentSequence);
                    output.add(token);

                    //CurrentSequence wil start with the last character to be read
                    previous = current;
                    currentSequence = new ByteSequence(previous);
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return output;
    }

    /**
     * Decodes the file into a list of byte sequences
     * @param dictionary - dictionary with the encoded-decoded pairs
     * @return list of decoded ByteSequences
     */
    private static List<ByteSequence> startDecoding(String path, LZWDictionary dictionary){
        List<ByteSequence> output = new ArrayList<>();
        try(FileInputStream is = new FileInputStream(path)){
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "windows-1252"));
            String s = br.readLine();
            while(s != null){
                String next;
                //The last line is the compression rate info, ignore it
                if((next = br.readLine()) == null)
                    break;
                ByteSequence sequence = dictionary.getSequence(s.getBytes());
                output.add(sequence);
                s = next;
            }
            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        return output;
    }
}
