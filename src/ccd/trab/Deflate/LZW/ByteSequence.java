package ccd.trab.Deflate.LZW;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ByteSequence {
    private final List<Byte> sequence = new ArrayList<>();

    ByteSequence(){}
    ByteSequence(byte b){
        this.append(b);
    }
    ByteSequence(byte[] sequence){
        this.append(sequence);
    }

    /**
     * Appends byte to the sequence
     * @param c byte to be appended
     */
    void append(byte c){
        this.sequence.add(c);
    }

    /**
     * Appends array of bytes to the sequence
     * @param b - array of bytes to be appended
     */
    void append(byte [] b){
        for (byte aB : b) {
            sequence.add(aB);
        }
    }

    /**
     * @return an immutable byte array that is equal to the current byte sequence
     */
    byte[] getBytes(){
        byte[] b = new byte[sequence.size()];
        for (int i = 0; i < sequence.size(); i++)
            b[i] = sequence.get(i);

        return b;
    }

    /**
     * Compares 2 instances of ByteSequence, by analyzing each byte
     * @param obj - Instance of ByteSequence
     * @return true if equal
     */
    @Override
    public boolean equals(Object obj) {
        byte[] b0 = this.getBytes();
        byte[] b1 = ((ByteSequence)obj).getBytes();

        return Arrays.equals(b0,b1);
    }

    @Override
    public String toString() {
        return new String(this.getBytes());
    }
}
